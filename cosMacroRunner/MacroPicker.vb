﻿Imports System.Reflection

Public Class MacroPicker

    Private assemblies As New List(Of Assembly)
    Private macroList As New List(Of String)

    ' make list of avaialble assembilies under app folder \\macros_dll
    Private Sub MakeAssemblyList()

        Dim path As String = AppDomain.CurrentDomain.BaseDirectory & "macros_dll"
        Dim files As String() = IO.Directory.GetFiles(path, "*.dll")

        For i = 0 To UBound(files)
            assemblies.Add(Assembly.LoadFile(files(i)))
        Next

    End Sub

    ' create available macro list to Display to user
    Private Sub MakeMacroList()

        For Each ass As Assembly In assemblies
            macroList.Add(ass.GetCustomAttribute(Of AssemblyTitleAttribute).Title)
        Next

    End Sub

    Private Sub MacroPicker_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        MakeAssemblyList()
        MakeMacroList()

        ListBox1.DataSource = macroList

    End Sub

    Private Sub btn_select_Click(sender As Object, e As EventArgs) Handles btn_select.Click
        Tag = assemblies(ListBox1.SelectedIndex)
        DialogResult = DialogResult.OK
    End Sub

    Private Sub btn_cancel_Click(sender As Object, e As EventArgs) Handles btn_cancel.Click
        If Not IsNothing(Tag) Then
            Tag = Nothing
        End If
        DialogResult = DialogResult.Cancel
    End Sub

    Private Sub btn_info_Click(sender As Object, e As EventArgs) Handles btn_info.Click
        MsgBox(assemblies(ListBox1.SelectedIndex).GetCustomAttribute(Of AssemblyDescriptionAttribute).Description)
    End Sub

End Class