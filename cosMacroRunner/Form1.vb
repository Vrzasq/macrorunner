﻿Imports AutConnListTypeLibrary
Imports AutConnMgrTypeLibrary
Imports dynamicClass
Imports System.Reflection

Public Class Form1

    Private connectionManager As New AutConnMgr
    Private connectionList As New AutConnList
    Private WithEvents macro As DynamicMacro

    Private activeConnections As New Dictionary(Of String, PCOMMConnection) From
        {{"A", New PCOMMConnection},
        {"B", New PCOMMConnection},
        {"C", New PCOMMConnection},
        {"D", New PCOMMConnection},
        {"E", New PCOMMConnection},
        {"F", New PCOMMConnection}}

    Private Sub timer_connectionCheck_Tick(sender As Object, e As EventArgs) Handles timer_connectionCheck.Tick
        CheckConnections()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        CheckConnections()
    End Sub


    Private Sub btn_runMacro_Click(sender As Object, e As EventArgs) Handles btn_runMacro.Click

        Dim connectionName = ComboBox1.SelectedItem.ToString()
        Dim macroPicker As New MacroPicker

        If macroPicker.ShowDialog() = DialogResult.OK Then

            '================= MAIN THREAD ================

            'Dim asm As Assembly = Assembly.LoadFile(dlg_openFile.FileName)
            'Dim fullTypeName = asm.FullName.Split(",")(0) & ".Macro"
            'macro = asm.CreateInstance(fullTypeName)
            'macro.INIT(connectionName)
            'ChangeConnectionState(connectionName, PCOMMConnectionSate.BUSY)
            'macro.Run()

            '================ END MAIN THREAD ===============


            '=============== BACKGROUND WORKER ================

            Dim asm As Assembly = CType(macroPicker.Tag, Assembly)
            Dim fullTypeName = asm.GetName().Name & ".Macro"
            macro = asm.CreateInstance(fullTypeName)
            ChangeConnectionState(connectionName, PCOMMConnectionSate.BUSY)

            'worker.RunWorkerAsync(New WorkerArgument With {
            '                      .ConnectionName = connectionName,
            '                      .Macro = macro})

            worker.RunWorkerAsync(connectionName)

            '============== END BACKGROUND WROKER ===============

        End If

    End Sub

    Private Sub worker_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles worker.DoWork

        Dim connectionName As String = CType(e.Argument, String)
        macro.INIT(connectionName)
        macro.Run()

    End Sub


    Private Sub macro_Complete(sender As Object, e As MacroEventArgs) Handles macro.Complete
        activeConnections(e.SesstionName).State = PCOMMConnectionSate.ACTIVE
        MsgBox(e.AdditionlInfo)
    End Sub

    ' Run throug connections list check connection state and set proper labels colors
    Private Sub CheckConnections()

        For Each con As KeyValuePair(Of String, PCOMMConnection) In activeConnections
            SetConnectionState(con.Key)
            SetConnecionLabel(con.Key)
        Next

        '  disable buttons if no valid connection
        ToggleMacroButtons(activeConnections.Where(Function(conState) conState.Value.State = PCOMMConnectionSate.ACTIVE).Count())
        SetValuesOnComboBox()

    End Sub

    ' Set label color according to connection state
    ' green ACTIVE
    ' yellow BUSY
    ' red INACTIVE
    Private Sub SetConnecionLabel(ByVal connectionName As String)

        Dim connectionPanel As Panel = pnl_connections.Controls.Find("pnl_connection" & connectionName, False)(0)

        If activeConnections(connectionName).State = PCOMMConnectionSate.ACTIVE Then
            connectionPanel.Controls(0).BackColor = Color.Green
        ElseIf activeConnections(connectionName).State = PCOMMConnectionSate.BUSY Then
            connectionPanel.Controls(0).BackColor = Color.Yellow
        Else
            connectionPanel.Controls(0).BackColor = Color.Red
        End If

    End Sub


    ' set initial connection state
    Private Sub SetConnectionState(ByVal connectionName As String)

        connectionList.Refresh()

        Dim connection As Object
        connection = connectionList.FindConnectionByName(connectionName)

        If activeConnections(connectionName).State <> PCOMMConnectionSate.BUSY Then
            If Not IsNothing(connection) Then
                activeConnections(connectionName).State = PCOMMConnectionSate.ACTIVE
            Else
                activeConnections(connectionName).State = PCOMMConnectionSate.INACTIVE
            End If
        End If

    End Sub

    '  disable buttons if no valid connection
    Private Sub ToggleMacroButtons(conCount As Integer)
        If conCount > 0 Then
            lbl_actieveConnectionsInfo.Visible = False
            btn_runMacro.Enabled = True
        Else
            lbl_actieveConnectionsInfo.Visible = True
            btn_runMacro.Enabled = False
        End If
    End Sub


    Private Sub SetValuesOnComboBox()

        Dim list As New List(Of String)

        For Each conn In activeConnections.Keys
            If activeConnections(conn).State = PCOMMConnectionSate.ACTIVE Then
                list.Add(conn)
            End If
        Next

        If IsNothing(ComboBox1.DataSource) Then
            ComboBox1.DataSource = list
        Else
            Dim dif = list.Except(ComboBox1.DataSource)
            If dif.Count() > 0 Or ComboBox1.DataSource.Count <> list.Count Then
                ComboBox1.DataSource = list
            End If
        End If

    End Sub

    Private Sub ChangeConnectionState(connectionName As String, state As PCOMMConnectionSate)
        activeConnections(connectionName).State = state
    End Sub


End Class