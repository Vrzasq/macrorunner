﻿Public Enum PCOMMConnectionSate
    ACTIVE
    INACTIVE
    BUSY
End Enum

Public Class PCOMMConnection

    Public Sub New()
        _state = PCOMMConnectionSate.INACTIVE
    End Sub

    Private _state As PCOMMConnectionSate
    Public Property State() As PCOMMConnectionSate
        Get
            Return _state
        End Get
        Set(ByVal value As PCOMMConnectionSate)
            _state = value
        End Set
    End Property


End Class
