﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.lbl_connection1 = New System.Windows.Forms.Label()
        Me.pnl_connectionA = New System.Windows.Forms.Panel()
        Me.lbl_statusA = New System.Windows.Forms.Label()
        Me.pnl_connectionB = New System.Windows.Forms.Panel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.pnl_connectionC = New System.Windows.Forms.Panel()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.pnl_connectionD = New System.Windows.Forms.Panel()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.pnl_connectionE = New System.Windows.Forms.Panel()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.pnl_connectionF = New System.Windows.Forms.Panel()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.pnl_connections = New System.Windows.Forms.Panel()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.lbl_actieveConnectionsInfo = New System.Windows.Forms.Label()
        Me.btn_runMacro = New System.Windows.Forms.Button()
        Me.lbl_chooseConnection = New System.Windows.Forms.Label()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.timer_connectionCheck = New System.Windows.Forms.Timer(Me.components)
        Me.worker = New System.ComponentModel.BackgroundWorker()
        Me.pnl_connectionA.SuspendLayout()
        Me.pnl_connectionB.SuspendLayout()
        Me.pnl_connectionC.SuspendLayout()
        Me.pnl_connectionD.SuspendLayout()
        Me.pnl_connectionE.SuspendLayout()
        Me.pnl_connectionF.SuspendLayout()
        Me.pnl_connections.SuspendLayout()
        Me.SuspendLayout()
        '
        'lbl_connection1
        '
        Me.lbl_connection1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lbl_connection1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.lbl_connection1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lbl_connection1.Location = New System.Drawing.Point(24, 0)
        Me.lbl_connection1.Name = "lbl_connection1"
        Me.lbl_connection1.Size = New System.Drawing.Size(98, 27)
        Me.lbl_connection1.TabIndex = 0
        Me.lbl_connection1.Text = "Connectin ""A"""
        Me.lbl_connection1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pnl_connectionA
        '
        Me.pnl_connectionA.Controls.Add(Me.lbl_statusA)
        Me.pnl_connectionA.Controls.Add(Me.lbl_connection1)
        Me.pnl_connectionA.Location = New System.Drawing.Point(3, 10)
        Me.pnl_connectionA.Name = "pnl_connectionA"
        Me.pnl_connectionA.Size = New System.Drawing.Size(200, 28)
        Me.pnl_connectionA.TabIndex = 1
        Me.pnl_connectionA.Tag = "A"
        '
        'lbl_statusA
        '
        Me.lbl_statusA.BackColor = System.Drawing.Color.Red
        Me.lbl_statusA.Location = New System.Drawing.Point(146, 7)
        Me.lbl_statusA.Name = "lbl_statusA"
        Me.lbl_statusA.Size = New System.Drawing.Size(26, 15)
        Me.lbl_statusA.TabIndex = 1
        Me.lbl_statusA.Tag = ""
        '
        'pnl_connectionB
        '
        Me.pnl_connectionB.Controls.Add(Me.Label1)
        Me.pnl_connectionB.Controls.Add(Me.Label2)
        Me.pnl_connectionB.Location = New System.Drawing.Point(3, 44)
        Me.pnl_connectionB.Name = "pnl_connectionB"
        Me.pnl_connectionB.Size = New System.Drawing.Size(200, 28)
        Me.pnl_connectionB.TabIndex = 1
        Me.pnl_connectionB.Tag = "B"
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.Red
        Me.Label1.Location = New System.Drawing.Point(146, 7)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(26, 15)
        Me.Label1.TabIndex = 1
        Me.Label1.Tag = ""
        '
        'Label2
        '
        Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.Label2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label2.Location = New System.Drawing.Point(24, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(98, 27)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Connectin ""B"""
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pnl_connectionC
        '
        Me.pnl_connectionC.Controls.Add(Me.Label3)
        Me.pnl_connectionC.Controls.Add(Me.Label4)
        Me.pnl_connectionC.Location = New System.Drawing.Point(3, 78)
        Me.pnl_connectionC.Name = "pnl_connectionC"
        Me.pnl_connectionC.Size = New System.Drawing.Size(200, 28)
        Me.pnl_connectionC.TabIndex = 1
        Me.pnl_connectionC.Tag = "C"
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.Color.Red
        Me.Label3.Location = New System.Drawing.Point(146, 7)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(26, 15)
        Me.Label3.TabIndex = 1
        '
        'Label4
        '
        Me.Label4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.Label4.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label4.Location = New System.Drawing.Point(24, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(98, 27)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "Connectin ""C"""
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pnl_connectionD
        '
        Me.pnl_connectionD.Controls.Add(Me.Label5)
        Me.pnl_connectionD.Controls.Add(Me.Label6)
        Me.pnl_connectionD.Location = New System.Drawing.Point(3, 112)
        Me.pnl_connectionD.Name = "pnl_connectionD"
        Me.pnl_connectionD.Size = New System.Drawing.Size(200, 28)
        Me.pnl_connectionD.TabIndex = 1
        Me.pnl_connectionD.Tag = "D"
        '
        'Label5
        '
        Me.Label5.BackColor = System.Drawing.Color.Red
        Me.Label5.Location = New System.Drawing.Point(146, 7)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(26, 15)
        Me.Label5.TabIndex = 1
        '
        'Label6
        '
        Me.Label6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.Label6.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label6.Location = New System.Drawing.Point(24, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(98, 27)
        Me.Label6.TabIndex = 0
        Me.Label6.Text = "Connectin ""D"""
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pnl_connectionE
        '
        Me.pnl_connectionE.Controls.Add(Me.Label7)
        Me.pnl_connectionE.Controls.Add(Me.Label8)
        Me.pnl_connectionE.Location = New System.Drawing.Point(3, 146)
        Me.pnl_connectionE.Name = "pnl_connectionE"
        Me.pnl_connectionE.Size = New System.Drawing.Size(200, 28)
        Me.pnl_connectionE.TabIndex = 1
        Me.pnl_connectionE.Tag = "E"
        '
        'Label7
        '
        Me.Label7.BackColor = System.Drawing.Color.Red
        Me.Label7.Location = New System.Drawing.Point(146, 7)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(26, 15)
        Me.Label7.TabIndex = 1
        '
        'Label8
        '
        Me.Label8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.Label8.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label8.Location = New System.Drawing.Point(24, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(98, 27)
        Me.Label8.TabIndex = 0
        Me.Label8.Text = "Connectin ""E"""
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pnl_connectionF
        '
        Me.pnl_connectionF.Controls.Add(Me.Label9)
        Me.pnl_connectionF.Controls.Add(Me.Label10)
        Me.pnl_connectionF.Location = New System.Drawing.Point(3, 180)
        Me.pnl_connectionF.Name = "pnl_connectionF"
        Me.pnl_connectionF.Size = New System.Drawing.Size(200, 28)
        Me.pnl_connectionF.TabIndex = 1
        Me.pnl_connectionF.Tag = "F"
        '
        'Label9
        '
        Me.Label9.BackColor = System.Drawing.Color.Red
        Me.Label9.Location = New System.Drawing.Point(146, 7)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(26, 15)
        Me.Label9.TabIndex = 1
        '
        'Label10
        '
        Me.Label10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.Label10.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label10.Location = New System.Drawing.Point(24, 0)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(98, 27)
        Me.Label10.TabIndex = 0
        Me.Label10.Text = "Connectin ""F"""
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pnl_connections
        '
        Me.pnl_connections.Controls.Add(Me.pnl_connectionF)
        Me.pnl_connections.Controls.Add(Me.pnl_connectionA)
        Me.pnl_connections.Controls.Add(Me.pnl_connectionE)
        Me.pnl_connections.Controls.Add(Me.pnl_connectionB)
        Me.pnl_connections.Controls.Add(Me.pnl_connectionD)
        Me.pnl_connections.Controls.Add(Me.pnl_connectionC)
        Me.pnl_connections.Location = New System.Drawing.Point(377, 44)
        Me.pnl_connections.Name = "pnl_connections"
        Me.pnl_connections.Size = New System.Drawing.Size(207, 224)
        Me.pnl_connections.TabIndex = 2
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(418, 306)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(134, 46)
        Me.Button1.TabIndex = 3
        Me.Button1.Text = "Check connections"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'lbl_actieveConnectionsInfo
        '
        Me.lbl_actieveConnectionsInfo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.lbl_actieveConnectionsInfo.Location = New System.Drawing.Point(401, 271)
        Me.lbl_actieveConnectionsInfo.Name = "lbl_actieveConnectionsInfo"
        Me.lbl_actieveConnectionsInfo.Size = New System.Drawing.Size(161, 29)
        Me.lbl_actieveConnectionsInfo.TabIndex = 4
        Me.lbl_actieveConnectionsInfo.Text = "No active connectios"
        Me.lbl_actieveConnectionsInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btn_runMacro
        '
        Me.btn_runMacro.Enabled = False
        Me.btn_runMacro.Location = New System.Drawing.Point(22, 77)
        Me.btn_runMacro.Name = "btn_runMacro"
        Me.btn_runMacro.Size = New System.Drawing.Size(124, 33)
        Me.btn_runMacro.TabIndex = 5
        Me.btn_runMacro.Text = "Run Macro"
        Me.btn_runMacro.UseVisualStyleBackColor = True
        '
        'lbl_chooseConnection
        '
        Me.lbl_chooseConnection.Location = New System.Drawing.Point(28, 122)
        Me.lbl_chooseConnection.Name = "lbl_chooseConnection"
        Me.lbl_chooseConnection.Size = New System.Drawing.Size(66, 35)
        Me.lbl_chooseConnection.TabIndex = 6
        Me.lbl_chooseConnection.Text = "Choose connection"
        Me.lbl_chooseConnection.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ComboBox1
        '
        Me.ComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(100, 128)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(121, 21)
        Me.ComboBox1.TabIndex = 7
        '
        'timer_connectionCheck
        '
        Me.timer_connectionCheck.Enabled = True
        Me.timer_connectionCheck.Interval = 2000
        '
        'worker
        '
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(596, 364)
        Me.Controls.Add(Me.ComboBox1)
        Me.Controls.Add(Me.lbl_chooseConnection)
        Me.Controls.Add(Me.btn_runMacro)
        Me.Controls.Add(Me.lbl_actieveConnectionsInfo)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.pnl_connections)
        Me.Name = "Form1"
        Me.Text = "Macro Runner"
        Me.pnl_connectionA.ResumeLayout(False)
        Me.pnl_connectionB.ResumeLayout(False)
        Me.pnl_connectionC.ResumeLayout(False)
        Me.pnl_connectionD.ResumeLayout(False)
        Me.pnl_connectionE.ResumeLayout(False)
        Me.pnl_connectionF.ResumeLayout(False)
        Me.pnl_connections.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents lbl_connection1 As Label
    Friend WithEvents pnl_connectionA As Panel
    Friend WithEvents lbl_statusA As Label
    Friend WithEvents pnl_connectionB As Panel
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents pnl_connectionC As Panel
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents pnl_connectionD As Panel
    Friend WithEvents Label5 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents pnl_connectionE As Panel
    Friend WithEvents Label7 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents pnl_connectionF As Panel
    Friend WithEvents Label9 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents pnl_connections As Panel
    Friend WithEvents Button1 As Button
    Friend WithEvents lbl_actieveConnectionsInfo As Label
    Friend WithEvents btn_runMacro As Button
    Friend WithEvents lbl_chooseConnection As Label
    Friend WithEvents ComboBox1 As ComboBox
    Friend WithEvents timer_connectionCheck As Timer
    Friend WithEvents worker As System.ComponentModel.BackgroundWorker
End Class
