﻿Public Class CursorPosition

    Private _col As Integer
    Public Property Col() As Integer
        Get
            Return _col
        End Get
        Set(ByVal value As Integer)
            _col = value
        End Set
    End Property

    Private _row As Integer
    Public Property Row() As Integer
        Get
            Return _row
        End Get
        Set(ByVal value As Integer)
            _row = value
        End Set
    End Property

End Class
