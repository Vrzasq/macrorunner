﻿Public Structure PCommButton

    Public Shared ReadOnly Property ENTER As String = "[ENTER]"
    Public Shared ReadOnly Property PAGE_DOWN As String = "[pagedn]"
    Public Shared ReadOnly Property PAGE_UP As String = "[pageup]"
    Public Shared ReadOnly Property NUM_PLUS As String = "[field+]"

    Public Shared ReadOnly Property F1 As String = "[PF1]"
    Public Shared ReadOnly Property F2 As String = "[PF2]"
    Public Shared ReadOnly Property F3 As String = "[PF3]"
    Public Shared ReadOnly Property F4 As String = "[PF4]"
    Public Shared ReadOnly Property F5 As String = "[PF5]"
    Public Shared ReadOnly Property F6 As String = "[PF6]"
    Public Shared ReadOnly Property F7 As String = "[PF7]"
    Public Shared ReadOnly Property F8 As String = "[PF8]"
    Public Shared ReadOnly Property F9 As String = "[PF9]"
    Public Shared ReadOnly Property F10 As String = "[PF10]"
    Public Shared ReadOnly Property F11 As String = "[PF11]"
    Public Shared ReadOnly Property F12 As String = "[PF12]"
    Public Shared ReadOnly Property F13 As String = "[PF13]"
    Public Shared ReadOnly Property F14 As String = "[PF14]"
    Public Shared ReadOnly Property F15 As String = "[PF15]"
    Public Shared ReadOnly Property F16 As String = "[PF16]"
    Public Shared ReadOnly Property F17 As String = "[PF17]"
    Public Shared ReadOnly Property F18 As String = "[PF18]"
    Public Shared ReadOnly Property F19 As String = "[PF19]"
    Public Shared ReadOnly Property F20 As String = "[PF20]"
    Public Shared ReadOnly Property F21 As String = "[PF21]"
    Public Shared ReadOnly Property F22 As String = "[PF22]"
    Public Shared ReadOnly Property F23 As String = "[PF23]"
    Public Shared ReadOnly Property F24 As String = "[PF24]"

End Structure
