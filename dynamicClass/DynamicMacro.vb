﻿Public MustInherit Class DynamicMacro

    Private runMacro As Boolean
    Private runExcel As Boolean
    Private mainShtName As String

    Protected pageID As String
    Protected session As Object
    Protected presentationSpace As Object
    Protected operationInformation As Object
    Protected fieldList As Object

    Protected excelApp As Object
    Protected mainWrk As Object
    Protected mainSht As Object

    Public Event Complete(sender As Object, eventArgs As MacroEventArgs)

    Public Name As String

    ' must be called before anything else
    Public Overridable Sub INIT(connectionName As String)

        runMacro = True
        PCommInit(connectionName)
        CheckPageID()

        If runMacro And runExcel Then ExcelInit(mainShtName)

    End Sub

    Public Sub Run()

        If runMacro Then

            MAIN()
            RaiseEvent Complete(Me, New MacroEventArgs With {
                                .SesstionName = session.Name,
                                .AdditionlInfo = "Done"})
        End If

    End Sub

    Protected Sub SetupExcelInitAttributes(runEx As Boolean, Optional shtName As String = "")
        runExcel = runEx
        mainShtName = shtName
    End Sub

    Protected MustOverride Sub MAIN()

    ' initilize required PCOMM compenent
    Private Sub PCommInit(connectionName As String)

        session = CreateObject("PCOMM.autECLSession")
        session.SetConnectionByName(connectionName)
        presentationSpace = session.autECLPS
        operationInformation = session.autECLOIA
        fieldList = presentationSpace.autECLFieldList

    End Sub

    ' initilize basic excel component for future use
    Private Sub ExcelInit(shtName As String)

        excelApp = CreateObject("Excel.Application")
        Dim file = excelApp.GetOpenFileName("Excel Files,*.xls;*.xlsx;*.xlsm;*.xlsb;*.csv")

        If file.ToString() <> "False" Then

            mainWrk = excelApp.Workbooks.Open(file)

            If shtName <> "" Then
                mainSht = mainWrk.Worksheets(shtName)
            Else
                mainSht = mainWrk.Worksheets(1)
            End If

            excelApp.Visible = True

        Else

            runMacro = False
            excelApp.Quit()
            RaiseEvent Complete(Me, New MacroEventArgs With {
                                .SesstionName = session.Name,
                                .AdditionlInfo = "File Missing"})
        End If


    End Sub

    Protected Sub ClearField(row As Integer, col As Integer)
        presentationSpace.SendKeys(PCommButton.NUM_PLUS, row, col)
    End Sub

    Protected Sub WaitForPCOMM(Optional ms As Integer = 0)

        If operationInformation.WaitForAppAvailable() Then
        End If
        If operationInformation.WaitForInputReady() Then
        End If

        If ms > 0 Then
            presentationSpace.Wait(ms)
        End If

    End Sub

    Private Sub CheckPageID()
        fieldList.Refresh()
        Dim id As String = GetFieldData(02, 072)
        If id <> pageID Then
            runMacro = False
            RaiseEvent Complete(Me, New MacroEventArgs With {
                                .SesstionName = session.Name,
                                .AdditionlInfo = "You can't run this macro on [page ID]: " & pageID})
        End If
    End Sub

    Protected Function GetFieldData(row As Integer, col As Integer) As String
        Return Trim(fieldList.FindFieldByRowCol(row, col).GetText())
    End Function

    Protected Function GetCursorPosition() As CursorPosition
        Return New CursorPosition() With {
            .Row = presentationSpace.CursorPosRow,
            .Col = presentationSpace.CursorPosCol}
    End Function

End Class