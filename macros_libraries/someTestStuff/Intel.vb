﻿Class Intel

    Private _waybill As String
    Public Property Waybill() As String
        Get
            Return _waybill
        End Get
        Set(ByVal value As String)
            _waybill = value
        End Set
    End Property

    Private _ctts As String
    Public Property Ctts() As String
        Get
            Return _ctts
        End Get
        Set(ByVal value As String)
            _ctts = value
        End Set
    End Property

End Class
