﻿Imports dynamicClass

Public Class Macro
    Inherits DynamicMacro

    Private data As Object

    Public Overrides Sub INIT(connectionName As String)

        pageID = "FFJB01C1"
        SetupExcelInitAttributes(True)
        MyBase.INIT(connectionName)

    End Sub

    Protected Overrides Sub MAIN()

        CleanCielWindow()

        Dim i As Integer = 2

        While mainSht.Cells(i, 1).Value2 <> ""

            'MsgBox("first " & mainSht.Cells(i, 1).Value2)

            If mainSht.Cells(i, 4).Value2 <> "" Then
                Continue While
            End If

            Dim shipmentInfo As New ShipmentInfo(i, mainSht)

            presentationSpace.SendKeys(PCommButton.F20)
            WaitForPCOMM()
            presentationSpace.SetText(shipmentInfo.STT, 08, 030)
            presentationSpace.SendKeys(PCommButton.ENTER)
            WaitForPCOMM()
            fieldList.Refresh()

            ' check for displayed STT numeber if dont match go to next
            Dim stt As String = GetFieldData(09, 009)
            If stt <> shipmentInfo.STT Then
                ExitFromSTTWindow(i)
                Continue While
            End If

            presentationSpace.SetText("1", 09, 005)
            presentationSpace.SendKeys(PCommButton.ENTER)
            WaitForPCOMM()

            ClearOptionFields()
            WaitForPCOMM()

            presentationSpace.SetText(20, 11, 002)
            presentationSpace.SendKeys(PCommButton.ENTER)
            WaitForPCOMM()

            presentationSpace.SetCursorPos(08, 020)
            WaitForPCOMM()

            presentationSpace.SendKeys(PCommButton.F10)
            WaitForPCOMM()

            CleanDeliveryToInfo()
            WaitForPCOMM()

            FillNewDeliveryInfo(shipmentInfo)
            WaitForPCOMM()

            ConfirmChanges()

            mainSht.Cells(i, 4).Value2 = "ok"

            i += 1
        End While


    End Sub

    Private Sub ConfirmChanges()
        For i = 0 To 5
            presentationSpace.SendKeys(PCommButton.ENTER)
            WaitForPCOMM()
        Next
    End Sub

    Private Sub FillNewDeliveryInfo(info As ShipmentInfo)
        presentationSpace.SetText(info.CompanyName, 15, 045)
        presentationSpace.SetText(info.Address, 17, 045)
    End Sub

    Private Sub ExitFromSTTWindow(i As Integer)
        mainSht.Cells(i, 4).Value2 = "not found"
        presentationSpace.SendKeys(PCommButton.F12)
        WaitForPCOMM()
    End Sub

    Private Sub ClearOptionFields()
        ClearField(10, 002)
        ClearField(10, 006)
        ClearField(10, 025)
        ClearField(11, 002)
        ClearField(13, 002)
        ClearField(15, 002)
        ClearField(17, 002)
        ClearField(19, 002)
    End Sub

    Private Sub CleanCielWindow()
        ClearField(03, 020)
        ClearField(04, 020)
        ClearField(10, 002)
        ClearField(10, 006)
        ClearField(10, 025)
    End Sub

    Private Sub CleanDeliveryToInfo()
        ClearField(15, 045)
        ClearField(16, 045)
        ClearField(17, 045)
    End Sub

End Class
