﻿Class ShipmentInfo

    Public Sub New(row As Integer, sht As Object)
        STT = Trim(sht.Cells(row, 1).Value2)
        CompanyName = Trim(sht.Cells(row, 2).Value2)
        Address = Trim(sht.Cells(row, 3).Value2)
    End Sub

    Private _isDone As String
    Public Property IsDone() As String
        Get
            Return _isDone
        End Get
        Set(ByVal value As String)
            _isDone = value
        End Set
    End Property

    Private _sst As String
    Public Property STT() As String
        Get
            Return _sst
        End Get
        Set(ByVal value As String)
            _sst = value
        End Set
    End Property

    Private _address As String
    Public Property Address() As String
        Get
            Return _address
        End Get
        Set(ByVal value As String)
            _address = value
        End Set
    End Property

    Private _companyName As String
    Public Property CompanyName() As String
        Get
            Return _companyName
        End Get
        Set(ByVal value As String)
            _companyName = value
        End Set
    End Property

End Class
